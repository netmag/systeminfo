import java.applet.*;
import java.awt.*;
import java.util.*;

class LabelField extends Panel {
    int labelWidth;
    Label lbl;
    TextField field;

    public LabelField(int labelWidth, String lbl, String val) {
	this.labelWidth = labelWidth;
	add(this.lbl = new Label(lbl));
	add(this.field = new TextField(val));
	field.setEditable(false);
    }
    public void layout() {
	Dimension d = size();

	Dimension p1 = lbl.preferredSize();
	Dimension p2 = field.preferredSize();
	lbl.reshape(0, 0, labelWidth, p1.height);
	field.reshape(labelWidth + 5, 0, d.width - (labelWidth + 5), p2.height);
    }
}

public class SystemInfo extends Applet {
    CardLayout c;
    Panel p;

   public final static String COPY="Public domain source was modified by Radim Kolar (hsn@cybermail.net) / NetMag Consulting."+
   "\nSee http://ncic.netmag.cz/apps/nase/systeminfo.html for latest version of this applet.";    
   
    public static void main(String[] args) {
    System.out.println(COPY);
    listsystemproperties();
    }

  public String getAppletInfo()
  {
   return COPY;
  }        
        
    public void init() {
	Font lbl = new Font("Helvetica", Font.BOLD, 14);
        Label l;
        l=new Label("System property lister",Label.CENTER);
        l.setBackground(Color.yellow);
        l.setFont(new Font("Helvetica",Font.BOLD,17));
        setBackground(Color.white);
	setLayout(new BorderLayout());
	add("South", p = new Panel());
        add("North", l);
	p.add(new Button("Previous"));
        p.add(new Button("Next"));

	add("Center", p = new Panel());
	p.setLayout(c = new CardLayout());

	Panel p2 = new Panel();
	p2.setLayout(new GridLayout(0, 1));
	p2.add(new Label("JVM Properties")).setFont(lbl);
	p2.add(new LabelField(100, "java.version:",    Property("java.version")));
	p2.add(new LabelField(100, "java.vendor:",     Property("java.vendor")));
	p2.add(new LabelField(100, "java.vendor.url:", Property("java.vendor.url")));
	p.add("system", p2);

	p2 = new Panel();
	p2.setLayout(new GridLayout(0, 1));
	p2.add(new Label("User Properties")).setFont(lbl);
	p2.add(new LabelField(100, "user.name:",    Property("user.name")));
	p2.add(new LabelField(100, "user.home:",    Property("user.home")));
	p2.add(new LabelField(100, "user.dir:", Property("user.dir")));
	p.add("user", p2);

	p2 = new Panel();
	p2.add(new Label("Java Properties")).setFont(lbl);
	p2.setLayout(new GridLayout(0, 1));
	p2.add(new LabelField(100, "java.home:",    Property("java.home")));
	p2.add(new LabelField(100, "java.compiler:",    Property("java.compiler")));
	p2.add(new LabelField(100, "awt.toolkit:",    Property("awt.toolkit")));
	p2.add(new LabelField(100, "java.class.version:", Property("java.class.version")));
	p2.add(new LabelField(100, "java.class.path:",    Property("java.class.path")));
	p.add("java", p2);

	p2 = new Panel();
	p2.setLayout(new GridLayout(0, 1));
	p2.add(new Label("OS Properties")).setFont(lbl);
	p2.add(new LabelField(100, "os.name:",    Property("os.name")));
	p2.add(new LabelField(100, "os.arch:",     Property("os.arch")));
	p2.add(new LabelField(100, "os.version:", Property("os.version")));
	p.add("os", p2);

	p2 = new Panel();
	p2.setLayout(new GridLayout(0, 1));
	p2.add(new Label("Misc Properties")).setFont(lbl);
	p2.add(new LabelField(100, "File.Separator:",    Property("file.separator")));
	p2.add(new LabelField(100, "Path.Separator:",    Property("path.separator")));
	p2.add(new LabelField(100, "Line.Separator:",    Property("line.separator")));
	p.add("sep", p2);

	p2 = new Panel();
	p2.setLayout(new GridLayout(0, 1));
	p2.add(new Label("Browser properties")).setFont(lbl);
	p2.add(new LabelField(100, "Document base:",    getDocumentBase().toString()));
	p2.add(new LabelField(100, "Code base:",        getCodeBase().toString()));
	p.add("br", p2);
                
        System.out.println("\nIf you see any security errors, DON'T PANIC! IT'S NORMAL!\n");
        System.out.println(COPY);        
    }

    private static String Property(String name)
    {
     String reply;
     try
     {
      reply=System.getProperty(name);
     }
     catch(SecurityException e) { reply="Property is blocked by Security Manager"; }
     
     if(reply==null) reply="(Null) Returned. Property doesn't exist?";
    
     return reply;
    }
    
    public boolean action(Event evt, Object obj) {
	if ("Next".equals(obj)) {
	    c.next(p);
	    return true;
	}
	if ("Previous".equals(obj)) {
	    c.previous(p);
	    return true;
	}
	return false;
    }
    
    private static void listsystemproperties()
    {
     Properties props;
     try{
     props=System.getProperties();
     }
     catch (SecurityException e) { System.out.println("Can't list system properties. Access Denied.");return;}
     Enumeration enum=props.propertyNames();
     for(;enum.hasMoreElements();)
     {
      String s;s=(String)enum.nextElement();
      System.out.println(s+" : "+Property(s));
     }

    }
}
